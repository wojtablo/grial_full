using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace OV3
{
	public partial class TimelinePage : ContentPage
	{
		public TimelinePage()
		{
			InitializeComponent();

			this.BindingContext = new TimelineViewModel();
		}
	}
}

