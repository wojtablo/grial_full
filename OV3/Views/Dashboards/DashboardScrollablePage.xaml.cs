using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace OV3
{
	public partial class DashboardScrollablePage : ContentPage
	{
		public DashboardScrollablePage()
		{
			InitializeComponent();

			BindingContext = new DashboardScrollableViewModel();
		}
	}

}
