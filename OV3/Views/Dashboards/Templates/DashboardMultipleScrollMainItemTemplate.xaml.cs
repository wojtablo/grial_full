using System;
using System.Collections.Generic;
using Xamarin.Forms;
using OV3.Resx;

namespace OV3
{
	public partial class DashboardMultipleScrollMainItemTemplate : ContentView
	{
		public DashboardMultipleScrollMainItemTemplate()
		{
			InitializeComponent();
		}

		public async void OnMovieTapped(object sender, EventArgs args)
		{
			await Application.Current.MainPage.DisplayAlert(
				AppResources.AlertTitleMovieTapped, 
				AppResources.AlertMessageMovieShouldPlayMovieNow, 
				AppResources.StringOK	
			);
		}
	}
}

