using Xamarin.Forms;

namespace OV3
{
	public partial class DashboardMultipleScrollPage : ContentPage
	{
		public DashboardMultipleScrollPage ()
		{
			InitializeComponent ();

			BindingContext = new DashboardMultipleScrollPageViewModel();
		}
	}
}
