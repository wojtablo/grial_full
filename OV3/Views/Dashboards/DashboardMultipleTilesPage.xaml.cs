using Xamarin.Forms;

namespace OV3
{
	public partial class DashboardMultipleTilesPage : ContentPage
	{
		public DashboardMultipleTilesPage ()
		{			
			InitializeComponent();

			BindingContext = new DashboardMutipleTilesViewModel();
		}
	}
}