using Xamarin.Forms;

namespace OV3
{
	public partial class ChatTimelinePage : ContentPage
	{
		public ChatTimelinePage()
		{
			InitializeComponent();
			BindingContext = new ChatViewModel(useRecent: false);
		}
	}
}
