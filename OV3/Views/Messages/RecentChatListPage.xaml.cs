using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace OV3
{
	public partial class RecentChatListPage : ContentPage
	{
		public RecentChatListPage()
		{
			InitializeComponent();

			BindingContext = new ChatViewModel(useRecent: true);
		}
	}
}

